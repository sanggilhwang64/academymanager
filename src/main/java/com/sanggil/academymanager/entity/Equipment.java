package com.sanggil.academymanager.entity;

import com.sanggil.academymanager.enums.Category;
import com.sanggil.academymanager.interfaces.CommonModelBuilder;
import com.sanggil.academymanager.model.EquipmentRequest;
import com.sanggil.academymanager.model.EquipmentUpdateRequest;
import com.sanggil.academymanager.model.QuantityUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private Category category;

    @Column(nullable = false)
    private Integer equipmentNumber;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false, length = 20)
    private String purchasePrice;

    @Column(nullable = false)
    private LocalDate purchaseDate;

    public void putEquipment(EquipmentUpdateRequest request) {
        this.name = request.getName();
    }

    public void putQuantity(QuantityUpdateRequest request) {
        this.quantity = request.getQuantity();
    }


    private Equipment(EquipmentBuilder builder) {
        this.equipmentNumber = builder.equipmentNumber;
        this.name = builder.name;
        this.quantity = builder.quantity;
        this.purchasePrice = builder.purchasePrice;
        this.purchaseDate = builder.purchaseDate;
    }

    public static class EquipmentBuilder implements CommonModelBuilder<Equipment> {
        private final Integer equipmentNumber;
        private final String name;
        private final Integer quantity;
        private final String purchasePrice;
        private final LocalDate purchaseDate;


        public EquipmentBuilder(EquipmentRequest request) {
            this.equipmentNumber = request.getEquipmentNumber();
            this.name = request.getName();
            this.quantity = request.getQuantity();
            this.purchasePrice = request.getPurchasePrice();
            this.purchaseDate = request.getPurchaseDate();
        }

        @Override
        public Equipment build() {
            return new Equipment(this);
        }
    }
}

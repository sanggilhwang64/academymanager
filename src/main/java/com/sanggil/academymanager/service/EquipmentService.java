package com.sanggil.academymanager.service;

import com.sanggil.academymanager.entity.Equipment;
import com.sanggil.academymanager.exception.CMissingDataException;
import com.sanggil.academymanager.model.*;
import com.sanggil.academymanager.repository.EquipmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    public void setEquipment(EquipmentRequest request) {
        Equipment addData = new Equipment.EquipmentBuilder(request).build();

        equipmentRepository.save(addData);
    }

    public ListResult<EquipmentItem> getEquipments() {
        List<Equipment> originList = equipmentRepository.findAll();

        List<EquipmentItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new EquipmentItem.EquipmentItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public EquipmentResponse getEquipment(long id) {
        Equipment originData = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new EquipmentResponse.EquipmentResponseBuilder(originData).build();
    }

    public void putEquipment(long id, EquipmentUpdateRequest request) {
        Equipment originData = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putEquipment(request);

        equipmentRepository.save(originData);
    }

    public void putQuantity(long id, QuantityUpdateRequest request) {
        Equipment originData = equipmentRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putQuantity(request);

        equipmentRepository.save(originData);
    }

    public void delEquipment(long id) {
        equipmentRepository.deleteById(id);
    }
}

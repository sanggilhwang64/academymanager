package com.sanggil.academymanager.model;

import com.sanggil.academymanager.entity.Equipment;
import com.sanggil.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentItem {
    private Long id;
    private String categoryName;
    private String equipmentName;

    private EquipmentItem(EquipmentItemBuilder builder) {
        this.id = builder.id;
        this.categoryName = builder.categoryName;
        this.equipmentName = builder.equipmentName;
    }

    public static class EquipmentItemBuilder implements CommonModelBuilder<EquipmentItem> {
        private final Long id;
        private final String categoryName;
        private final String equipmentName;

        public EquipmentItemBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.categoryName = equipment.getCategory().getName();
            this.equipmentName = equipment.getName();
        }
        @Override
        public EquipmentItem build() {
            return new EquipmentItem(this);
        }
    }
}

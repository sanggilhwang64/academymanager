package com.sanggil.academymanager.model;

import com.sanggil.academymanager.enums.Category;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class EquipmentRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Category category;

    @NotNull
    private Integer equipmentNumber;

    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    private Integer quantity;

    @NotNull
    @Length(min = 5, max = 20)
    private String purchasePrice;

    @NotNull
    private LocalDate purchaseDate;


}

package com.sanggil.academymanager.model;

import com.sanggil.academymanager.entity.Equipment;
import com.sanggil.academymanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class EquipmentResponse {
    private Long id;
    private String name;
    private Integer equipmentNumber;
    private Integer quantity;
    private String purchasePrice;

    private EquipmentResponse(EquipmentResponseBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.equipmentNumber = builder.equipmentNumber;
        this.quantity = builder.quantity;
        this.purchasePrice = builder.purchasePrice;
    }

    public static class EquipmentResponseBuilder implements CommonModelBuilder<EquipmentResponse> {
        private final Long id;
        private final String name;
        private final Integer equipmentNumber;
        private final Integer quantity;
        private final String purchasePrice;

        public EquipmentResponseBuilder(Equipment equipment) {
            this.id = equipment.getId();
            this.name = equipment.getName();
            this.equipmentNumber = equipment.getEquipmentNumber();
            this.quantity = equipment.getQuantity();
            this.purchasePrice = equipment.getPurchasePrice();
        }

        @Override
        public EquipmentResponse build() {
            return new EquipmentResponse(this);
        }
    }
}

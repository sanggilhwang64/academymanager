package com.sanggil.academymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class EquipmentUpdateRequest {
    @NotNull
    @Length(min = 2, max = 10)
    private String name;
}

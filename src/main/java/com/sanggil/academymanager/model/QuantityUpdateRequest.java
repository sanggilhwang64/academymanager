package com.sanggil.academymanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class QuantityUpdateRequest {
    @NotNull
    private Integer quantity;
}

package com.sanggil.academymanager.controller;

import com.sanggil.academymanager.model.*;
import com.sanggil.academymanager.service.EquipmentService;
import com.sanggil.academymanager.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/equipment")
public class EquipmentController {
    private final EquipmentService equipmentService;

    @PostMapping("/data")
    public CommonResult setEquipment(@RequestBody @Valid EquipmentRequest request) {
        equipmentService.setEquipment(request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<EquipmentItem> getEquipments() {
        return ResponseService.getListResult(equipmentService.getEquipments(),true);
    }

    @GetMapping("/detail/{id}")
    public SingleResult<EquipmentResponse> getEquipment(@PathVariable long id) {
        return ResponseService.getSingleResult(equipmentService.getEquipment(id));
    }

    @PutMapping("/name/{id}")
    public CommonResult putEquipment(@PathVariable long id, @RequestBody @Valid EquipmentUpdateRequest request) {
        equipmentService.putEquipment(id, request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/quantity/{id}")
    public CommonResult putQuantity(@PathVariable long id, @RequestBody @Valid QuantityUpdateRequest request) {
        equipmentService.putQuantity(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delEquipment(@PathVariable long id) {
        equipmentService.delEquipment(id);

        return ResponseService.getSuccessResult();
    }
}

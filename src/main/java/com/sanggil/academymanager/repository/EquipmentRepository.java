package com.sanggil.academymanager.repository;

import com.sanggil.academymanager.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
}

package com.sanggil.academymanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
